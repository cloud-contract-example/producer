package com.example.producer.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class ProducerController {

    @GetMapping("/validate/even")
    public String checkEvenOdd(@RequestParam Integer num) {
        return num % 2 == 0 ? "Even" : "Odd";
    }
}
